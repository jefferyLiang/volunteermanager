package org.jliang.mngt.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;


import org.hibernate.annotations.GenericGenerator;

import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="volunteer_tb")
public class Volunteer {
    @Id
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    @Column(name = "person_id")
	private long id;
    private String username;
	
    @Column(name = "phone_number")
    private String phoneNumber;
    private String email;
    
    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(	name = "vo_skill", 
	joinColumns = { @JoinColumn(name = "person_id") }, 
	inverseJoinColumns = { @JoinColumn(name = "skill_id") })
    private Set<Skill> skillSet;
 
    public Volunteer(String username, String phoneNumber, String email) {
        this.username = username;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.skillSet = new HashSet<Skill>();
    }
 
    public void addSkill(Skill skill) {
        this.skillSet.add(skill);
    }
    
    // setters and getters
    public long getId() {
		return id;
	}
    
	public void setId(long id) {
		this.id = id;
	}
    
	@Column
	public String getUsername() {
		return username;
	}
	

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}
	
	@Column(name = "email")
	public void setEmail(String email) {
		this.email = email;
	}
    
	public Set<Skill> getSkillSet() {
		return skillSet;
	}
	
	public void setSkillSet(Set<Skill> skillSet) {
		this.skillSet = skillSet;
	}
	
}