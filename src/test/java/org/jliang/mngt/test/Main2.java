package org.jliang.mngt.test;



import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.jliang.mngt.model.StudentEntity;
import org.jliang.mngt.model.GroupEntity;


public class Main2 {
	 private static List listSkills() {
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        session.beginTransaction();
	        List result = session.createQuery("from Skill").list();
	        session.getTransaction().commit();
	        return result;
	}
	 
	public static void main(String[] args) {

		// Set up database tables
//		HibernateUtil.droptable("drop table PERSON");
//		HibernateUtil.setup("create table PERSON ( id int NOT NULL AUTO_INCREMENT, cname VARCHAR(20), PRIMARY KEY(id))");
//		HibernateUtil2.droptable("drop table student");
//		HibernateUtil2.setup("create table student ( id int NOT NULL AUTO_INCREMENT, grade VARCHAR(20), school VARCHAR(20), PRIMARY KEY(id))");

		// Create SessionFactory and Session object
		
		//Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().openSession();

		// Perform life-cycle operations under a transaction
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			
			// Create another Person object and save it.
			GroupEntity g1 = new GroupEntity();
			g1.setGroupName("XeroName");
			GroupEntity g2 = new GroupEntity();
			g1.setGroupName("Zuma_Name");			
			// Create a Person object and save it
			StudentEntity s1 = new StudentEntity();
			s1.setSname("Zhang1");
			StudentEntity s2 = new StudentEntity();
			s2.setSname("Lee2");
			StudentEntity s3 = new StudentEntity();
			s3.setSname("Jack");

			Set<StudentEntity> StudentSet = new HashSet<StudentEntity>();
			StudentSet.add(s1);
			StudentSet.add(s2);
			g1.setStudents(StudentSet);
			
			StudentSet.clear();
			StudentSet.add(s3);
			g2.setStudents(StudentSet);
			
			session.save(s1);
			session.save(s2);
			session.save(g1);	


//			// Retrieve the person objects
//			Person person = (Person)session. get(Person.class, p1.getId());
//			System.out.println("First person retrieved = " + person.getName());
//			person = (Person)session.get(Person.class, p2.getId());
//			System.out.println("Second person retrieved = " + person.getName());

//			Student s1 = new Student();
//			s1.setSchool("zzzz01");
//			s1.setGrade("Z");
//			session.save(s1);
//			
//			Student student = (Student)session. get(Student.class, s1.getId());
//            System.out.println("First student retrieved = " + student.getId() + ", " +
//                               "School attending = " + student.getSchool() + ", " +
//                               "Grade = " + student.getGrade());
			tx.commit();
			//tx = null;
			System.out.println("Successfully");
		} catch ( HibernateException e ) {
			if ( tx != null ) tx.rollback();
			e.printStackTrace();
		} finally {
			//session.close();
			session = null;
		}

		// Display tables
//		session = HibernateUtil.getSessionFactory().getCurrentSession();
//		HibernateUtil.checkData("select * from volunteer_tb");
//		HibernateUtil.checkData("select * from skill_tb");

//		List<Skill> skills = listSkills();
//		for (Skill item: skills) {
//			System.out.println("--------");
//			System.out.println("item:descr:" + item.getDescription());
//			System.out.println("item:Category:" + item.getCategoryCode());
//		}
	}
}

/*
 * that Hibernate is using �thread� managed sessions. With this type of 
 * session management Hibernate manages the session for you. 
 * When you first attempt to use a session Hibernate will create one 
 * and attach it to your local thread. When you commit the transaction 
 * in the session Hibernate will automatically close the session meaning it can�t be reused.
 * 
 * current_session_context_class� to �managed
 * 
 * org.hibernate.classic.Session session = HibernateUtil.getSessionFactory().openSession();
   session.setFlushMode(FlushMode.MANUAL);
   ManagedSessionContext.bind(session);
   session.beginTransaction();

ManagedSessionContext.unbind(HibernateUtil.getSessionFactory());
session.flush();
session.getTransaction().commit();
session.close();
 */