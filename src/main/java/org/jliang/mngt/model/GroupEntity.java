package org.jliang.mngt.model;

import java.util.HashSet;
import java.util.Set;
import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

@Entity
@Table(name="group_tb")
public class GroupEntity implements Serializable{
	//(strategy=GenerationType.AUTO)
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="ID")
	int id;
	
	@Column(name="group_name")
	String GroupName;
	
	@OneToMany(cascade=CascadeType.ALL) //, mappedBy="groupItem"
	@JoinColumn(name="groupEntity_ID")
	Set<StudentEntity> StudentEntities;
	public GroupEntity() {
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGroupName() {
		return GroupName;
	}

	public void setGroupName(String groupName) {
		GroupName = groupName;
	}

	public Set<StudentEntity> getStudents() {
		return StudentEntities;
	}

	public void setStudents(Set<StudentEntity> studentEntities) {
		StudentEntities = studentEntities;
	}

}