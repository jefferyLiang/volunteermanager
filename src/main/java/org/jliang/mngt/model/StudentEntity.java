package org.jliang.mngt.model;
import java.util.Set;


import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

//(strategy=GenerationType.AUTO)
//uniqueConstraints ={ @UniqueConstraint(columnNames ="student_id")})
@Entity
@Table(name = "student_tb") 
public class StudentEntity implements Serializable {
//	private static final long serialVersionUID = -6790693372846798580L;
//
//	@Id 
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@Column(name = "student_id", unique = true, nullable = false)
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "student_id")
	Integer id;
	
	@Column(name = "student_name", unique = true, nullable = false)
	String sname;
	
	@ManyToOne
	private GroupEntity groupEntity;
	
	public StudentEntity () {
		
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSname() {
		return sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	public GroupEntity getGroup() {
		return groupEntity;
	}

	public void setGroup(GroupEntity groupEntity) {
		this.groupEntity = groupEntity;
	}
	
}

 