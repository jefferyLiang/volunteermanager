package org.jliang.mngt.test;



import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.jliang.mngt.model.Skill;
import org.jliang.mngt.model.Volunteer;


public class Main {
	 private static List listSkills() {
	        Session session = HibernateUtil.getSessionFactory().openSession();
	        session.beginTransaction();
	        List result = session.createQuery("from Skill").list();
	        session.getTransaction().commit();
	        return result;
	}
	 
	public static void main(String[] args) {

		// Set up database tables
//		HibernateUtil.droptable("drop table PERSON");
//		HibernateUtil.setup("create table PERSON ( id int NOT NULL AUTO_INCREMENT, cname VARCHAR(20), PRIMARY KEY(id))");
//		HibernateUtil2.droptable("drop table student");
//		HibernateUtil2.setup("create table student ( id int NOT NULL AUTO_INCREMENT, grade VARCHAR(20), school VARCHAR(20), PRIMARY KEY(id))");

		// Create SessionFactory and Session object
		
		//Session session = HibernateUtil.getSessionFactory().openSession();
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();

		// Perform life-cycle operations under a transaction
		Transaction tx = null;
		try {
			tx = session.beginTransaction();

			// Create a Person object and save it
			Volunteer p1 = new Volunteer("Sang Shin", "Sang Shin", "Sang Shin");

			// Create another Person object and save it.
			Skill s1 = new Skill();
			s1.setCategoryCode(1001);
			s1.setDescription("Java");
			
			Skill s2 = new Skill();
			s2.setCategoryCode(110);
			s2.setDescription("Database");
			
			p1.addSkill(s1);
			p1.addSkill(s2);
			
			session.save(p1);	
			session.save(s1);
			session.save(s2);

//			// Retrieve the person objects
//			Person person = (Person)session. get(Person.class, p1.getId());
//			System.out.println("First person retrieved = " + person.getName());
//			person = (Person)session.get(Person.class, p2.getId());
//			System.out.println("Second person retrieved = " + person.getName());

//			Student s1 = new Student();
//			s1.setSchool("zzzz01");
//			s1.setGrade("Z");
//			session.save(s1);
//			
//			Student student = (Student)session. get(Student.class, s1.getId());
//            System.out.println("First student retrieved = " + student.getId() + ", " +
//                               "School attending = " + student.getSchool() + ", " +
//                               "Grade = " + student.getGrade());
			tx.commit();
			//tx = null;
			System.out.println("Successfully");
		} catch ( HibernateException e ) {
			if ( tx != null ) tx.rollback();
			e.printStackTrace();
		} finally {
			//session.close();
			session = null;
		}

		// Display tables
		session = HibernateUtil.getSessionFactory().getCurrentSession();
		HibernateUtil.checkData("select * from volunteer_tb");
		HibernateUtil.checkData("select * from skill_tb");

		List<Skill> skills = listSkills();
		for (Skill item: skills) {
			System.out.println("--------");
			System.out.println("item:descr:" + item.getDescription());
			System.out.println("item:Category:" + item.getCategoryCode());
		}
	}
}

/*
 * that Hibernate is using �thread� managed sessions. With this type of 
 * session management Hibernate manages the session for you. 
 * When you first attempt to use a session Hibernate will create one 
 * and attach it to your local thread. When you commit the transaction 
 * in the session Hibernate will automatically close the session meaning it can�t be reused.
 * 
 * current_session_context_class� to �managed
 * 
 * org.hibernate.classic.Session session = HibernateUtil.getSessionFactory().openSession();
   session.setFlushMode(FlushMode.MANUAL);
   ManagedSessionContext.bind(session);
   session.beginTransaction();

ManagedSessionContext.unbind(HibernateUtil.getSessionFactory());
session.flush();
session.getTransaction().commit();
session.close();
 */