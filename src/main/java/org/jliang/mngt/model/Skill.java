package org.jliang.mngt.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "skill_tb")
public class Skill {
    @Id
    @GeneratedValue
    @Column(name = "skill_id")
    long id;
    
    @Column(name = "category_code")
    int categoryCode;
    
    @Column(name = "description")
    String description;
    
    @ManyToMany(mappedBy = "skillSet")
    Set<Volunteer> personSet;
 
    public Skill() {
 
    }
 
    public Skill(int skillLevel, String description) {
        this.categoryCode = skillLevel;
        this.description = description;
    }
 
    public long getId() {
        return id;
    }
 
    public void setId(long id) {
        this.id = id;
    }
 
    public int getCategoryCode() {
        return categoryCode;
    }
 
    public void setCategoryCode(int categoryCode) {
        this.categoryCode = categoryCode;
    }
 
    public String getDescription() {
        return description;
    }
 
    public void setDescription(String town) {
        this.description = description;
    }


    public Set<Volunteer> getPersonSet() {
        return personSet;
    }
 
    public void setPersonSet(Set<Volunteer> personSet) {
        this.personSet = personSet;
    }
 
}